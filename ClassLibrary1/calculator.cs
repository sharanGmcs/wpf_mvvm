﻿namespace ClassLibrary1
{
    using NUnit.Framework;
    [TestFixture]
    public class calculator
    {
        [Test]
        public void Add()
        {
            int result = sample.calc.Add(6, 6);
            var expected = 12;
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void Add1()
        {
            int result = sample.calc.Add(7, 6);
            var expected = 13;
            Assert.AreEqual(expected, result);
        }

    }
}
